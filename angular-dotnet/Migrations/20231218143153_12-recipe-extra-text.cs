﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace angular_dotnet.Migrations
{
    public partial class _12recipeextratext : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "tressero_np");

            migrationBuilder.RenameTable(
                name: "StoreSection",
                newName: "StoreSection",
                newSchema: "tressero_np");

            migrationBuilder.RenameTable(
                name: "RecipeIngredientJunction",
                newName: "RecipeIngredientJunction",
                newSchema: "tressero_np");

            migrationBuilder.RenameTable(
                name: "Recipe",
                newName: "Recipe",
                newSchema: "tressero_np");

            migrationBuilder.RenameTable(
                name: "Ingredient",
                newName: "Ingredient",
                newSchema: "tressero_np");

            migrationBuilder.AddColumn<string>(
                name: "RecipeText",
                schema: "tressero_np",
                table: "Recipe",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RecipeText",
                schema: "tressero_np",
                table: "Recipe");

            migrationBuilder.RenameTable(
                name: "StoreSection",
                schema: "tressero_np",
                newName: "StoreSection");

            migrationBuilder.RenameTable(
                name: "RecipeIngredientJunction",
                schema: "tressero_np",
                newName: "RecipeIngredientJunction");

            migrationBuilder.RenameTable(
                name: "Recipe",
                schema: "tressero_np",
                newName: "Recipe");

            migrationBuilder.RenameTable(
                name: "Ingredient",
                schema: "tressero_np",
                newName: "Ingredient");
        }
    }
}
