# Migration SQL Generation

Use below for initial migration (not using any previous history)
E.g. if you want to delete all migrations in ./Migrations, you could create a Single migration to instantiate current EF model with this command.
```bash
# Set some variables (bash) - note, this is the name that comes after the YYYYMMDDTTTTTT
$INIT_MIGRATION_NAME='InitialCreate'
DATE_NOW=$(TZ=UTC date +"%Y%m%d%H%M") # E.g. 202210272247, same format as migrations used for up.sql, down.sql

# Generate Migration Files, using e.g. "num-issue-desc" as INIT_MIGRATION_NAME
dotnet ef migrations add "$INIT_MIGRATION_NAME" -c GrocerContext --configuration Debug --verbose --output-dir "Migrations"

# Generate up, down SQL scripts to run an upgrade manually (NOTE: files may take a while to appear)
dotnet ef migrations script "$INIT_MIGRATION_NAME" -c GrocerContext --configuration Debug --verbose -o ./Migrations/SQL/"$DATE_NOW"_"$NEW_MIGRATION_NAME".up.sql

```

Use below for any updates to the model. Make sure to change the FROM/TO variables, discluding the prepended datetime
```bash

# Set some variables for migrations
PREV_MIGRATION_NAME='InitialSeed'
NEW_MIGRATION_NAME='12-recipe-extra-text' 
DATE_NOW=$(TZ=UTC date +"%Y%m%d%H%M")

# Generate Migration Files
cd angular-dotnet/
dotnet ef migrations add "$NEW_MIGRATION_NAME" -c GrocerContext --configuration Debug --verbose --output-dir "Migrations"

# Generate up, down SQL scripts to run an upgrade manually (NOTE: files may take a while to appear)
dotnet ef migrations script "$PREV_MIGRATION_NAME" "$NEW_MIGRATION_NAME" -c GrocerContext --configuration Debug --verbose -o ./Migrations/SQL/"$DATE_NOW"_"$NEW_MIGRATION_NAME".up.sql
dotnet ef migrations script "$NEW_MIGRATION_NAME" "$PREV_MIGRATION_NAME" -c GrocerContext --configuration Debug --verbose -o ./Migrations/SQL/"$DATE_NOW"_"$NEW_MIGRATION_NAME".down.sql

```