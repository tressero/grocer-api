﻿START TRANSACTION;

ALTER TABLE tressero_np."Recipe" DROP COLUMN "RecipeText";

DELETE FROM "__EFMigrationsHistory"
WHERE "MigrationId" = '20231218143153_12-recipe-extra-text';

COMMIT;

