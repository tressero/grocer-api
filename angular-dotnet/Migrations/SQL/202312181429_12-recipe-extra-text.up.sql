﻿DO $EF$
    BEGIN
        IF NOT EXISTS(SELECT 1 FROM pg_namespace WHERE nspname = 'tressero_np') THEN
            CREATE SCHEMA tressero_np;
        END IF;
    END $EF$;

/** NON-PROD **/
START TRANSACTION;

ALTER TABLE tressero_np."Recipe" ADD "RecipeText" text NULL;

INSERT INTO tressero_np."__EFMigrationsHistory" ("MigrationId", "ProductVersion")
VALUES ('20231218143153_12-recipe-extra-text', '6.0.12');

COMMIT;


/** PROD */
START TRANSACTION;

ALTER TABLE public."Recipe" ADD "RecipeText" text NULL;

INSERT INTO public."__EFMigrationsHistory" ("MigrationId", "ProductVersion")
VALUES ('20231218143153_12-recipe-extra-text', '6.0.12');

COMMIT;

